# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns


#Fetch read
# tmp = np.load(r'/home/erdi/Desktop/Storage/Publications/Hindsight_goal_generation/code_comments/log/accs/ddpg-FetchSlide-v1-hgg-(2021-05-17-21:14:56)/interval.npz')  
tmp = np.load(r'/home/erdi/Desktop/Storage/silinecek/logs/log_normal_dist/accs/HGG_fetch_slide/HGG_fetch_slide-ddpg-FetchSlide-v1-hgg-(2021-05-28-00:08:57)/interval.npz')  
tmp = tmp['info']

tmp1 = np.load(r'/home/erdi/Desktop/Storage/silinecek/logs/log_uniform_dist/accs/HGG_fetch_slide/HGG_fetch_slide-ddpg-FetchSlide-v1-hgg-(2021-05-28-13:39:46)/interval.npz')  
tmp1 = tmp1['info']

# #Fetch push
# pd_frame2 = pd.read_csv(r'/home/erdi/Desktop/Storage/silinecek/exp3/r0/progress.csv')  
# train_success_rate2 = np.array(pd_frame2['test/success_rate'])

# # FetchPickAndPlace
# pd_frame3 = pd.read_csv(r'/home/erdi/Desktop/Storage/silinecek/FetchPickAndPlace/cpu1ep50/alg=DDPG+CHER=/r2/progress.csv')  
# train_success_rate3 = np.array(pd_frame3['test/success_rate'])

# pd_frame4 = pd.read_csv(r'/home/erdi/Desktop/Storage/silinecek/FetchSlide/cpu1ep50/alg=DDPG+CHER=/r3/progress.csv')  
# train_success_rate4 = np.array(pd_frame4['test/success_rate'])

episode = tmp[:,0]

results = tmp[:,1] - tmp1[:,1]
fig = plt.figure()

xdata = episode
#xdata = np.array([0,1,2,3,4,5,6])
sns.tsplot(time=xdata, data=results, color="r", linestyle="--")
# sns.tsplot(time=xdata, data=results[1], color="g", linestyle="--")
# sns.tsplot(time=xdata, data=results[2], color="b", linestyle=":")
# sns.tsplot(time=xdata, data=results[3], color="y", linestyle="-.")
sns.tsplot(time=xdata, data=np.ones(len(xdata)), color="b", linestyle="-")
plt.ylabel("Success Rate", fontsize=15)
plt.xlabel("Episode ", fontsize=15, labelpad=4)

plt.title("Difference Learning accuracy with $N(\mu,\sigma) - U(a,b)$", fontsize=15)

plt.legend(labels=['FetchSlide'])
# plt.ylim([0, 1.1])
plt.savefig('FetchSlide_difference_acc.png')

plt.show()