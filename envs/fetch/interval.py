import gym
import numpy as np
from .fixobj import FixedObjectGoalEnv

class IntervalGoalEnv(FixedObjectGoalEnv):
	def __init__(self, args):
		FixedObjectGoalEnv.__init__(self, args)

	def generate_goal(self):
		if self.has_object:
			goal = self.initial_gripper_xpos[:3] + self.target_offset
			if self.args.env=='FetchSlide-v1':
				random_sampled_value = np.random.uniform(-self.target_range, self.target_range) * 0.5
				goal[1] += random_sampled_value
				if (np.random.uniform(0, 1)) > 0.5:
					goal[0] += self.target_range*0.5
				else:
					goal[0] -= self.target_range * 0.5

			else:
				random_sampled_value = np.random.uniform(-self.target_range, self.target_range)
				goal[0] += random_sampled_value
				if (np.random.uniform(0, 1)) > 0.9:
					goal[1] += self.target_range
				else:
					goal[1] -= (self.target_range + 0.1)
			goal[2] = self.height_offset + int(self.target_in_the_air)*0.45
		else:
			 random_sampled_value = np.random.uniform(-self.target_range, self.target_range)
			 if (np.random.uniform(0,1)) > 0.5:
				 goal = self.initial_gripper_xpos[:3] + np.array([random_sampled_value, self.target_range, self.target_range])
			 else:
				 goal = self.initial_gripper_xpos[:3] + np.array([random_sampled_value, -self.target_range, self.target_range])

		# site_id = self.sim.model.site_name2id('goal_area_site1')
		# self.sim.model.site_pos[site_id] = [goal[0],goal[1]-random_sampled_value,goal[2]]
		#
		# site_id = self.sim.model.site_name2id('goal_area_site2')
		# self.sim.model.site_pos[site_id] = [goal[0]-self.target_range, goal[1] - random_sampled_value, goal[2]]

		return goal.copy()