import numpy as np
from envs import make_env
from algorithm.replay_buffer import goal_based_process
from utils.os_utils import make_dir
from common import get_args
import re
from ast import literal_eval
import tensorflow as tf
import os
from gym.wrappers.monitoring.video_recorder import VideoRecorder


class Player:
    def __init__(self, args):
        # initialize environment
        self.args = args
        self.env = make_env(args)
        self.args.timesteps = self.env.max_episode_steps
        self.env_test = make_env(args)
        self.info = []
        self.test_rollouts = 100

        # get current policy from path (restore tf session + graph)
        self.play_dir = args.play_path
        self.play_epoch = args.play_epoch
        self.meta_path = os.path.join(self.play_dir, "saved_policy-{}.meta".format(self.play_epoch))
        self.sess = tf.Session()
        self.saver = tf.train.import_meta_graph(self.meta_path)
        self.saver.restore(self.sess, tf.train.latest_checkpoint(self.play_dir))
        graph = tf.get_default_graph()
        self.raw_obs_ph = graph.get_tensor_by_name("raw_obs_ph:0")
        self.pi = graph.get_tensor_by_name("main/policy/net/pi/Tanh:0")

    def my_step_batch(self, obs):
        # compute actions from obs based on current policy by running tf session initialized before
        actions = self.sess.run(self.pi, {self.raw_obs_ph: obs})
        return actions

    def play(self):
        # play policy on env
        env = self.env
        acc_sum, obs = 0.0, []
        for i in range(self.test_rollouts):
            obs.append(goal_based_process(env.reset()))
            for timestep in range(self.args.timesteps):
                actions = self.my_step_batch(obs)
                obs, infos = [], []
                ob, reward, done, info  = env.step(actions[0])
                obs.append(goal_based_process(ob))
                infos.append(info)
                print("Reward:" ,reward)
                if not (reward<0):
                    print("stop")
                env.render()






if __name__ == "__main__":
    # Call play.py in order to see current policy progress
    #python play.py --env FetchSlide-v1 --goal custom --play_path policies/FetchReach/1310-ddpg-FetchReach-v1-hgg-stop/ --play_epoch best
    args = get_args()
    print("------")
    args.env = "FetchReach-v1"
    # args.goal = "custom"
    args.play_path = "/home/erdi/Desktop/Storage/Publications/HGG/code_comments/report/goal_sampled_area/results/log_different_goals_fetchreach/"
    args.play_epoch = "latest"
    player = Player(args)
    player.play()
   
    # player.demoRecordPushJoints()
    # player.demoRecordReach()
    #player.demoRecordReach1()
    # player.demoRecordPickNoObstacle()
    # player.demoRecordPickAndPlaceObstacle()

