#!/usr/bin/env python
import gym
from gym import envs

env = gym.make("FetchReach-v1")
obs = env.reset()
tmp = env.action_space.sample()
for _ in range(100):
    env.render(mode='human')

    obs = env.step(tmp)
env.close()

